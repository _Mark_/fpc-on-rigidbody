﻿using System;
using FPC.Input;

namespace FPC.States
{
  public class StateController : IDisposable
  {
    private bool _isJumpStart;
    private readonly IJumpInput _jumpInput;
    private readonly StateMachine _stateMachine;
    private readonly GroundDetector _groundDetector;
    private readonly FirstPersonControllerMono _firstPersonController;

    public StateController(
      IJumpInput jumpInput,
      StateMachine stateMachine,
      GroundDetector groundDetector,
      FirstPersonControllerMono firstPersonController)
    {
      _jumpInput = jumpInput;
      _stateMachine = stateMachine;
      _groundDetector = groundDetector;
      _firstPersonController = firstPersonController;
    }

    public void Initialize()
    {
      _jumpInput.JumpPerformed += OnJumpPerformed;
    }

    private void OnJumpPerformed()
    {
      if (IsGroundAvailable())
        _isJumpStart = true;
    }

    public void Update()
    {
      bool isSlopeAvailable = IsSlopeAvailable();
      bool isGroundAvailable = IsGroundAvailable();

      if (isGroundAvailable)
      {
        if (isSlopeAvailable)
        {
          EnterTo<Displacement>();
        }
        else // isSlopeAvailable == false;
        {
          EnterTo<RollingDownTheSlopeSate>();
        }
      }
      else // isGroundAvailable == false;
      {
        EnterTo<Flight>();
        return;
      }
      
      if (_isJumpStart)
      {
        _isJumpStart = false;
        EnterTo<Jump>();
      }
    }

    public void Dispose()
    {
      _jumpInput.JumpPerformed -= OnJumpPerformed;
    }

    private void EnterTo<TType>()
    {
      if (_stateMachine.CurrentStatus != typeof(TType))
        _stateMachine.EnterTo<TType>();
    }

    private bool IsGroundAvailable() =>
      _groundDetector.IsGroundAvailable;

    private bool IsSlopeAvailable() => 
      _groundDetector.Slope <= _firstPersonController.AvailableSlope;
  }
}