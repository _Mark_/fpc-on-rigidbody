﻿using System;
using UnityEngine;

namespace FPC.States
{
  public class Displacement : State
  {
    private readonly Rigidbody _rigidBody;
    private readonly HorizontalMovement _horizontalMovement;

    public Displacement(Rigidbody rigidBody, HorizontalMovement horizontalMovement)
    {
      Status = GetType();
      _rigidBody = rigidBody;
      _horizontalMovement = horizontalMovement;
    }

    public override Type Status { get; }

    public override void Execute()
    {
      _rigidBody.velocity = _horizontalMovement.SpeedAlongTheSurface();
    }
  }
}