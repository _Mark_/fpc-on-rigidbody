﻿using System;
using UnityEngine;

namespace FPC.States
{
  public class RollingDownTheSlopeSate : State
  {
    private readonly Rigidbody _rigidBody;
    private readonly GroundDetector _groundDetector;
    private readonly HorizontalMovement _horizontalMovement;
    private readonly RollingDownTheSlope _rollingDownTheSlope;

    public RollingDownTheSlopeSate(
      Rigidbody rigidBody,
      GroundDetector groundDetector,
      HorizontalMovement horizontalMovement,
      RollingDownTheSlope rollingDownTheSlope)
    {
      Status = GetType();
      _rigidBody = rigidBody;
      _groundDetector = groundDetector;
      _horizontalMovement = horizontalMovement;
      _rollingDownTheSlope = rollingDownTheSlope;
    }

    public override Type Status { get; }

    public override void Execute()
    {
      var horizontalMovement = _horizontalMovement.Speed();
      horizontalMovement = Vector3.ProjectOnPlane(horizontalMovement, _groundDetector.GroundNormal);
      _rigidBody.velocity = _rollingDownTheSlope.RollDown() + horizontalMovement;
    }
  }
}