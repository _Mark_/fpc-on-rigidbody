﻿using System;
using UnityEngine;

namespace FPC.States
{
  public class Jump : State
  {
    private Vector3 _horizontalSpeed;
    private readonly FPC.Jump _jump;
    private readonly Transform _pivot;
    private readonly Rigidbody _rigidbody;

    public Jump(FPC.Jump jump, Transform pivot, Rigidbody rigidbody)
    {
      Status = GetType();
      _jump = jump;
      _pivot = pivot;
      _rigidbody = rigidbody;
    }

    public override Type Status { get; }

    protected override void OnEnter()
    {
      _horizontalSpeed = Vector3.ProjectOnPlane(_rigidbody.velocity, PivotUp());
      _rigidbody.velocity = _horizontalSpeed + PivotUp() * _jump.StartSpeed;
    }

    public override void Execute()
    {
      
    }

    private Vector3 PivotUp() => 
      _pivot.transform.up;
  }
}