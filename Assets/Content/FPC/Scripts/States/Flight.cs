﻿using System;
using UnityEngine;

namespace FPC.States
{
  public class Flight : State
  {
    private Vector3 _horizontalSpeed;
    private Vector3 _verticalSpeed;
    private readonly Transform _pivot;
    private readonly Rigidbody _rigidBody;
    private readonly Gravitation _gravitation;
    private readonly FirstPersonControllerMono _firstPersonController;

    public Flight(
      Transform pivot,
      Rigidbody rigidBody,
      Gravitation gravitation,
      FirstPersonControllerMono firstPersonController)
    {
      Status = GetType();
      _pivot = pivot;
      _rigidBody = rigidBody;
      _gravitation = gravitation;
      _firstPersonController = firstPersonController;
    }

    public override Type Status { get; }

    protected override void OnEnter()
    {
      _gravitation.StopFall();
      _verticalSpeed = Vector3.Project(_rigidBody.velocity, PivotUp());
      _horizontalSpeed = Vector3.ProjectOnPlane(_rigidBody.velocity, PivotUp());
    }

    public override void Execute()
    {
      float deltaTime = Time.fixedDeltaTime;
      _horizontalSpeed -= _horizontalSpeed.normalized * Resistance() * deltaTime;
      _verticalSpeed += _gravitation.Speed(deltaTime);
      _rigidBody.velocity = _verticalSpeed + _horizontalSpeed;
    }

    protected override void OnExit()
    {
      _rigidBody.velocity = Vector3.zero;
    }

    private float Resistance() => 
      _firstPersonController.ResistanceToHorizontalMovementInFlight;

    private Vector3 PivotUp() => _pivot.up;
  }
}