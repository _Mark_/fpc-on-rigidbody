﻿using System;
using System.Collections.Generic;
using System.Linq;
using FPC.Input;
using Sirenix.OdinInspector;
using UnityEngine;

namespace FPC.States
{
  [Serializable]
  public class StateMachine : IDisposable
  {
#if UNITY_EDITOR
    [ReadOnly, SerializeField] private string _debugStatus;
#endif

    private State _currentState;
    private readonly StateController _stateController;
    private readonly Dictionary<Type, State> _states;

    public StateMachine(
      FPC.Jump jump,
      Transform pivot,
      Rigidbody rigidBody,
      IJumpInput jumpInput,
      Gravitation gravitation,
      GroundDetector groundDetector,
      HorizontalMovement horizontalMovement,
      RollingDownTheSlope rollingDownTheSlope,
      FirstPersonControllerMono firstPersonController)
    {
      _stateController = new StateController(jumpInput, this, groundDetector, firstPersonController);

      _states = new Dictionary<Type, State>()
      {
        [typeof(Jump)] = new Jump(jump, pivot, rigidBody),
        [typeof(Flight)] = new Flight(pivot, rigidBody, gravitation, firstPersonController),
        [typeof(Displacement)] = new Displacement(rigidBody, horizontalMovement),
        [typeof(RollingDownTheSlopeSate)] = new RollingDownTheSlopeSate(rigidBody, groundDetector, horizontalMovement, rollingDownTheSlope),
      };
    }

    public Type CurrentStatus => _currentState.Status;

    public void Initialize()
    {
      _stateController.Initialize();
      EnterTo<Displacement>();
      _stateController.Update();
    }

    public void Execute()
    {
      _stateController.Update();
      _currentState.Execute();
    }

    public void Dispose() =>
      _stateController.Dispose();

    public void EnterTo<TType>()
    {
      var newStatus = typeof(TType);

#if UNITY_EDITOR
      _debugStatus = newStatus.ToString().Split('.').Last();
#endif

      _currentState?.Exit();
      _currentState = _states[newStatus];
      _currentState.Enter();
    }
  }
}