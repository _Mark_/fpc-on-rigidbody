﻿using System;

namespace FPC.States
{
  public abstract class State
  {
    public abstract Type Status { get; }
    public abstract void Execute();

    protected virtual void OnEnter() { }
    protected virtual void OnExit() { }

    public void Enter() => OnEnter();

    public void Exit() => OnExit();
  }
}