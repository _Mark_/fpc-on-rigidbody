﻿using System;
using UnityEngine;

namespace FPC
{
  [Serializable]
  public class Jump
  {
    [SerializeField] private float _startSpeed = 10;

    public float StartSpeed => _startSpeed;
  }
}