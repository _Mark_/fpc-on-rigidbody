﻿using System;
using FPC.Input;
using UnityEngine;

namespace FPC
{
  [Serializable]
  public class HorizontalMovement
  {
    [SerializeField] private float _speed = 10;
    
    private IMovementInput _movementInput;
    private Transform _horizontalHeadPivot;
    private GroundDetector _groundDetector;

    public void Initialize(
      IMovementInput movementInput,
      Transform horizontalHeadPivot,
      GroundDetector groundDetector)
    {
      _movementInput = movementInput;
      _groundDetector = groundDetector;
      _horizontalHeadPivot = horizontalHeadPivot;
    }

    public Vector3 Direction()
    {
      var movementVector = new Vector3(_movementInput.X, 0, _movementInput.Y);
      movementVector = _horizontalHeadPivot.TransformVector(movementVector);
      return movementVector.normalized;
    }

    public Vector3 Speed()
    {
      var movementVector = Direction();
      return movementVector * _speed;
    }

    public Vector3 SpeedAlongTheSurface()
    {
      var movementVector = new Vector3(_movementInput.X, 0, _movementInput.Y);
      movementVector = _horizontalHeadPivot.TransformVector(movementVector);
      movementVector = Vector3.ProjectOnPlane(movementVector, _groundDetector.GroundNormal);
      movementVector.Normalize();
      return movementVector * _speed;
    }
  }
}