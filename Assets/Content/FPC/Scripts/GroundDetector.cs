﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace FPC
{
  [Serializable]
  public class GroundDetector
  {
    [SerializeField] private LayerMask _layerMask;
    [Space]
    [ReadOnly, SerializeField] private bool isGroundAvailable;
    [ReadOnly, SerializeField] private float _slope;
    [ReadOnly, SerializeField] private Vector3 _groundNormal;

    private Transform _pivot;
    private CapsuleCollider _capsuleCollider;
    private const float _groundSensitive = 0.01f;
    private const float _reducingTheDetectorRadius = 0.001f;
    private readonly RaycastHit[] _hits = new RaycastHit[5];

    public void Initialize(
      Transform pivot,
      CapsuleCollider capsuleCollider)
    {
      _pivot = pivot;
      _capsuleCollider = capsuleCollider;
    }

    public RaycastHit Hit { get; private set; }
    public float Slope => _slope;
    public Vector3 GroundNormal => _groundNormal;
    public bool IsGroundAvailable => isGroundAvailable;

    public void Update()
    {
      int hitsQuantity = SphereCastNonAlloc(_hits);
      if (hitsQuantity > 0)
      {
        isGroundAvailable = true;
        Hit = NearestHit(hitsQuantity, _hits);
        _groundNormal = ClarifyNormal(); // TODO: is it necessary?
        _slope = Vector3.Angle(_groundNormal, PivotUp());
      }
      else
      {
        isGroundAvailable = false;
      }
    }

    private int SphereCastNonAlloc(RaycastHit[] hits)
    {
      Vector3 raycastOrigin = _pivot.position + PivotUp() * _capsuleCollider.center.y;
      float radius = _capsuleCollider.radius - _reducingTheDetectorRadius;
      float maxDistance = _capsuleCollider.height / 2 - radius + _groundSensitive;
      return Physics.SphereCastNonAlloc(raycastOrigin, radius, PivotDown(), hits, maxDistance, _layerMask);
    }

    private RaycastHit NearestHit(int hitsQuantity, RaycastHit[] hits)
    {
      RaycastHit previousHit = hits[0];
      float distanceToPreviousHit = Mathf.Infinity;

      for (var i = 0; i < hitsQuantity; i++)
      {
        var hit = hits[i];
        if (distanceToPreviousHit > hit.distance)
        {
          previousHit = hit;
          distanceToPreviousHit = hit.distance;
        }
      }

      return previousHit;
    }

    private Vector3 ClarifyNormal()
    {
      var raycastOrigin = Hit.point + PivotUp();
      var ray = new Ray(raycastOrigin, PivotDown());

      if (Hit.collider.Raycast(ray, out var additionalHit, _capsuleCollider.height - _capsuleCollider.radius))
      {
        if (Vector3.Angle(additionalHit.normal, PivotUp()) < 89)
        {
          return additionalHit.normal;
        }
      }

      return Hit.normal;
    }

    private Vector3 PivotUp() =>
      _pivot.up;

    private Vector3 PivotDown() =>
      -_pivot.up;
  }
}