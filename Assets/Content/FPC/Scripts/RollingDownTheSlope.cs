﻿using System;
using UnityEngine;

namespace FPC
{
  [Serializable]
  public class RollingDownTheSlope
  {
    private Transform _pivot;
    private Gravitation _gravitation;
    private GroundDetector _groundDetector;
    private const int _maxSlope = 90;

    public void Initialize(
      Transform pivot,
      Gravitation gravitation,
      GroundDetector groundDetector)
    {
      _pivot = pivot;
      _gravitation = gravitation;
      _groundDetector = groundDetector;
    }

    public Vector3 RollDown()
    {
      Vector3 directionOfTheSlope = DirectionOfTheSlope();
      Vector3 movementVector = new Vector3(directionOfTheSlope.x, directionOfTheSlope.y, directionOfTheSlope.z);
      float rollDownSpeed = _groundDetector.Slope * _gravitation.MaxSpeed / _maxSlope;
      return movementVector * rollDownSpeed;
    }
    
    private Vector3 DirectionOfTheSlope()
    {
      Vector3 projectOnPivotFlat = Vector3.ProjectOnPlane(_groundDetector.GroundNormal, _pivot.up);
      Vector3 directionOfTheSlope = Vector3.ProjectOnPlane(projectOnPivotFlat, _groundDetector.GroundNormal);
      return directionOfTheSlope.normalized;
    }
  }
}