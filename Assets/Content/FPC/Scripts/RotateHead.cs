﻿using System;
using FPC.Input;
using UnityEngine;

namespace FPC
{
  [Serializable]
  public class RotateHead
  {
    [SerializeField] private float _min = -90;
    [SerializeField] private float _max = 90;

    private Transform _target;
    private Transform _horizontal;
    private IMouseDeltaPosition _mouseDeltaPosition;

    private float _xOffset;
    private float _yOffset;
    private Quaternion _startRotation;

    public void Initialize(Transform target, Transform horizontal, IMouseDeltaPosition mouseDeltaPosition)
    {
      _target = target;
      _horizontal = horizontal;
      _startRotation = target.rotation;
      _mouseDeltaPosition = mouseDeltaPosition;
    }

    public void Rotate()
    {
      _xOffset += _mouseDeltaPosition.X * Time.deltaTime * Sensitive();
      _yOffset += _mouseDeltaPosition.Y * Time.deltaTime * Sensitive();

      _yOffset = Mathf.Clamp(_yOffset, _min, _max);

      _target.rotation = _startRotation *
                         Quaternion.AngleAxis(_xOffset, Vector3.up) *
                         Quaternion.AngleAxis(_yOffset, Vector3.right);

      _horizontal.localRotation = Quaternion.Euler(0, _target.localRotation.eulerAngles.y, 0);
    }

    private float Sensitive() => 
      _mouseDeltaPosition.Sensitive;
  }
}