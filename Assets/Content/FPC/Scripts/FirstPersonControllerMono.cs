using System;
using FPC.Input;
using FPC.States;
using UnityEngine;

namespace FPC
{
  [RequireComponent(typeof(Rigidbody))]
  public class FirstPersonControllerMono : MonoBehaviour, IDisposable
  {
    [SerializeField] private StateMachine _stateMachine;
    [Space]
    [SerializeField] private Transform _headPivot;
    [SerializeField] private Transform _horizontalHeadPivot;
    [SerializeField] private CapsuleCollider _capsuleCollider;
    [Space]
    [SerializeField] private float _availableSlope = 50;
    [SerializeField] private float _resistanceToHorizontalMovementInFlight = 5;
    [SerializeField] private Jump _jump;
    [SerializeField] private RotateHead _rotateHead;
    [SerializeField] private Gravitation _gravitation;
    [SerializeField] private GroundDetector _groundDetector;
    [SerializeField] private HorizontalMovement _horizontalMovement;

    private Rigidbody _rigidBody;
    private IJumpInput _jumpInput;
    private IMovementInput _movementInput;
    private IMouseDeltaPosition _mouseDeltaPosition;
    private RollingDownTheSlope _rollingDownTheSlope;
    private readonly StartSettings _startSettings = new StartSettings();

    public float AvailableSlope => _availableSlope;
    public float ResistanceToHorizontalMovementInFlight => _resistanceToHorizontalMovementInFlight;

    private void OnDestroy() => 
      Dispose();

    public void ResolveDependencies(
      IJumpInput jumpInput,
      IMovementInput movementInput,
      IMouseDeltaPosition mouseDeltaPosition)
    {
      _jumpInput = jumpInput;
      _movementInput = movementInput;
      _mouseDeltaPosition = mouseDeltaPosition;
    }
    
    public void Initialize()
    {
      _rigidBody = GetComponent<Rigidbody>();

      _rotateHead.Initialize(_headPivot, _horizontalHeadPivot, _mouseDeltaPosition);
      _gravitation.Initialize(transform);
      _startSettings.Initialize(_rigidBody);
      _groundDetector.Initialize(transform, _capsuleCollider);
      RollingDownTheSlopeInitialize();
      _horizontalMovement.Initialize(_movementInput, _horizontalHeadPivot, _groundDetector);
      StateMachineInitialize();
    }

    public void Dispose() => 
      _stateMachine.Dispose();

    private void FixedUpdate()
    {
      _groundDetector.Update();
      _stateMachine.Execute();
    }

    private void LateUpdate()
    {
      _rotateHead.Rotate();
    }

    private void StateMachineInitialize()
    {
      _stateMachine = new StateMachine(_jump, transform, _rigidBody, _jumpInput, _gravitation, _groundDetector,
        _horizontalMovement, _rollingDownTheSlope, this);
      _stateMachine.Initialize();
    }

    private void RollingDownTheSlopeInitialize()
    {
      _rollingDownTheSlope = new RollingDownTheSlope();
      _rollingDownTheSlope.Initialize(transform, _gravitation, _groundDetector);
    }
  }
}