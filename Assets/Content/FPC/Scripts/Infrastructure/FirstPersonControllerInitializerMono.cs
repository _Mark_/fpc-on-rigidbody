using FPC.Input;
using UnityEngine;

namespace FPC.Infrastructure
{
    public class FirstPersonControllerInitializerMono : MonoBehaviour
    {
        [SerializeField] private InputService _inputService;
        [SerializeField] private FirstPersonControllerMono _firstPersonController;
        
        private void Start()
        {
            _firstPersonController.ResolveDependencies(_inputService, _inputService, _inputService);
            _firstPersonController.Initialize();
        }
    }
}