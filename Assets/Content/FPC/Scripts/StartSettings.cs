﻿using UnityEngine;

namespace FPC
{
  public class StartSettings
  {
    public void Initialize(Rigidbody rigidbody)
    {
      rigidbody.drag = 0;
      rigidbody.useGravity = false;
      rigidbody.isKinematic = false;
      rigidbody.freezeRotation = true;
      rigidbody.interpolation = RigidbodyInterpolation.Interpolate;
    }
  }
}