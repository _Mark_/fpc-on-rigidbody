﻿using System;
using UnityEngine;

namespace FPC
{
  [Serializable]
  public class Gravitation
  {
    [SerializeField] private AnimationCurve _speed;
    [SerializeField] private float _maxSpeed = 50;
    [SerializeField] private float _duration = 2;
    
    private float _timer;
    private float _currentSpeed;
    private Transform _pivot;

    public float MaxSpeed => _maxSpeed;

    public void Initialize(Transform pivot) => 
      _pivot = pivot;

    public Vector3 Speed(float deltaTime)
    {
      _timer += deltaTime;
      _currentSpeed = _maxSpeed * _speed.Evaluate(_timer / _duration);
      return -_pivot.up * _currentSpeed;
    }

    public void StopFall() => 
      _timer = 0;
  }
}