﻿using System;

namespace FPC.Input
{
  public interface IJumpInput
  {
    event Action JumpPerformed;
    event Action JumpCanceled;
  }
}