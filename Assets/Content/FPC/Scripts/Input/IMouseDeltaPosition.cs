﻿using Shared;

namespace FPC.Input
{
  public interface IMouseDeltaPosition : IService
  {
    float Sensitive { get; }
    float X { get; }
    float Y { get; }
  }
}