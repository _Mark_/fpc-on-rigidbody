using System;
using UnityEngine;

namespace FPC.Input
{
  public class InputService : MonoBehaviour, IMovementInput, IMouseDeltaPosition, IJumpInput
  {
    [SerializeField] private float _sensitive;

    private GameInput _gameInput;
    private Action _onJumpPerformed;
    private Action _onJumpCanceled;

    public float Sensitive => _sensitive;
    float IMovementInput.X => _gameInput.FPC.Movement.ReadValue<Vector2>().x;
    float IMovementInput.Y => _gameInput.FPC.Movement.ReadValue<Vector2>().y;

    float IMouseDeltaPosition.X => _gameInput.FPC.View.ReadValue<Vector2>().x;
    float IMouseDeltaPosition.Y => -_gameInput.FPC.View.ReadValue<Vector2>().y;

    public event Action JumpPerformed
    {
      add => _onJumpPerformed += value;
      remove => _onJumpPerformed -= value;
    }
    public event Action JumpCanceled
    {
      add => _onJumpCanceled += value;
      remove => _onJumpCanceled -= value;
    }

    private void OnEnable() =>
      EnableFPCMap();

    private void OnDisable() =>
      DisableFPCMap();

    private void Awake()
    {
      Initialize();
    }

    public void Initialize()
    {
      _gameInput = new GameInput();
      EnableFPCMap();

      _gameInput.FPC.Jump.performed += (_) => _onJumpPerformed?.Invoke();
      _gameInput.FPC.Jump.canceled += (_) => _onJumpCanceled?.Invoke();
    }

    private void EnableFPCMap()
    {
      if (_gameInput.FPC.enabled == false)
        _gameInput.FPC.Enable();
    }

    private void DisableFPCMap()
    {
      if (_gameInput.FPC.enabled)
        _gameInput.Disable();
    }
  }
}