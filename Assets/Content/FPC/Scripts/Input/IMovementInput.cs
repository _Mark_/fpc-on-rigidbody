﻿using Shared;

namespace FPC.Input
{
  public interface IMovementInput : IService
  {
    float X { get; }
    float Y { get; }
  }
}